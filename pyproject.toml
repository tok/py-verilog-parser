#
# Copyright (c) 2018-2024 T. {Benz,Kramer}.
#
# This file is part of verilog-parser 
# (see https://codeberg.org/tok/py-verilog-parser).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

# SPDX-FileCopyrightText: 2024 Thomas Kramer
# SPDX-FileCopyrightText: 2018-2021 T. {Benz, Kramer}
#
# SPDX-License-Identifier: GPL-3.0-or-later

[project]
name = "verilog-parser"
version = "0.0.5"
description="Parser for structural verilog."
readme = {file = "README.md", content-type="text/markdown"}
license = {text = "AGPL-3.0-or-later"}

requires-python = ">=3.7"

keywords= ["verilog", "parser"]

authors = [ 
  {name = "T. Kramer", email = "code@tkramer.ch"},
  {name = "T. Benz", email = "dont@spam.me"}
]


classifiers = [
  'License :: OSI Approved :: GNU Affero General Public License v3',
  'Development Status :: 3 - Alpha',
  'Topic :: Scientific/Engineering',
  'Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)',
  'Programming Language :: Python :: 3'
]

dependencies = [
  'lark>=1.1'
]

[project.urls]
'Homepage' = 'https://codeberg.org/tok/py-verilog-parser'
'Repository' = 'https://codeberg.org/tok/py-verilog-parser'
'Issue Tracker' = 'https://codeberg.org/tok/py-verilog-parser/issues'

[tool.setuptools.packages.find]
where = ["src"]

[build-system]
# These are the assumed default build requirements from pip:
# https://pip.pypa.io/en/stable/reference/pip/#pep-517-and-518-support
requires = ["setuptools>=43.0.0"]
build-backend = "setuptools.build_meta"
